<?php
$rdet = $this->db
->where(COL_IDRPJMD, $data[COL_UNIQ])
->get(TBL_MRPJMD_DET)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <?=form_open_multipart(current_url(), array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
          <div class="card-body">
            <div class="form-group">
              <label>URAIAN</label>
              <textarea class="form-control" name="<?=COL_SASARANNAMA?>" required><?=!empty($data)?$data[COL_SASARANNAMA]:''?></textarea>
            </div>

            <div class="form-group">
              <label>INDIKATOR</label>
              <table class="table table-bordered table-hover">
                <thead>
                  <tr>
                    <th style="width: 10px; white-space: nowrap">No.</th>
                    <th>Indikator</th>
                    <th>Satuan</th>
                    <th style="width: 150px; white-space: nowrap">Target</th>
                    <th class="text-center" style="width: 10px; white-space: nowrap">
                      <a href="<?=site_url('site/master/rpjmd-add-det/'.$data[COL_UNIQ])?>" class="btn btn-primary btn-xs btn-add"><i class="far fa-plus-circle"></i></a>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach($rdet as $r) {
                    ?>
                    <tr>
                      <td style="width: 10px; white-space: nowrap"><?=$r[COL_INDIKATORNO]?></td>
                      <td><?=$r[COL_INDIKATORURAIAN]?></td>
                      <td><?=$r[COL_INDIKATORSATUAN]?></td>
                      <td><?=$r[COL_INDIKATORTARGET]?></td>
                      <td>
                        <a href="<?=site_url('site/master/rpjmd-delete-det/'.$r[COL_UNIQ])?>" class="btn btn-danger btn-xs btn-delete"><i class="far fa-times-circle"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer">
            <a href="<?=site_url('site/master/rpjmd')?>" class="btn btn-secondary btn-sm"><i class="far fa-arrow-circle-left"></i>&nbsp;KEMBALI</a>&nbsp;
            <button type="submit" class="btn btn-primary btn-sm"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
          </div>
          <?=form_close()?>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-add" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title font-weight-bold">TAMBAH INDIKATOR</span>
      </div>
      <form id="form-rpjmd" action="<?=current_url()?>" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="form-group">
            <div class="row">
              <div class="col-sm-3">
                <label>NO.</label>
                <input type="number" class="form-control" placeholder="No." name="<?=COL_INDIKATORNO?>" />
              </div>
            </div>
          </div>
          <div class="form-group">
            <label>INDIKATOR</label>
            <textarea class="form-control" rows="2" placeholder="Uraian" name="<?=COL_INDIKATORURAIAN?>"></textarea>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col-sm-6">
                <label>SATUAN</label>
                <input type="text" class="form-control" placeholder="Satuan" name="<?=COL_INDIKATORSATUAN?>" />
              </div>
              <div class="col-sm-6">
                <label>TARGET</label>
                <input type="text" class="form-control" placeholder="Target" name="<?=COL_INDIKATORTARGET?>" />
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
          <button type="submit" id="btn-save" class="btn btn-sm btn-success"><i class="far fa-arrow-circle-right"></i>&nbsp;SIMPAN</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('.btn-add').click(function() {
    var url = $(this).attr('href');
    $('#form-rpjmd', $('#modal-add')).attr('action', url);
    $('#modal-add').modal('show');
    return false;
  });
  $('.btn-delete').click(function() {
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
        }
      }, "json").done(function() {
        location.reload();
      }).fail(function() {
        toastr.error('SERVER ERROR');
      });
    }
    return false;
  });

  $('#form-rpjmd').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      //btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            location.reload();
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          //btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });

  $('#form-main').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.redirect) {
              location.href = res.redirect;
            } else {
              setTimeout(function(){
                location.reload();
              }, 1000);
            }
          }
        },
        error: function(data) {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
