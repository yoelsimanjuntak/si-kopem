<?php
class Ajax extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
    setlocale (LC_TIME, 'id_ID');
  }

  function now() {
    $dt = json_encode(array(
      'Day'=>date('D'),
      'Date'=>date('d'),
      'Month'=>date('M'),
      'Year'=>date('Y'),
      'Hour'=>date('H'),
      'Minute'=>date('i'),
      'Second'=>date('s')
    ));
    echo $dt;
  }
}
