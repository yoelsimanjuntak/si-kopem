<?php
class Skpd extends MY_Controller {

  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      show_error('TIDAK MEMILIKI HAK AKSES!');
      exit();
    }
  }

  public function index() {
    $data['title'] = "SKPD";
    $this->template->load('backend', 'site/skpd/index', $data);
  }

  public function index_load() {
    $ruser = GetLoggedUser();
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];

    $orderdef = array(COL_SKPDNAMA=>'asc');
    $orderables = array(null,COL_SKPDNAMA,COL_SKPDNAMAPIMPINAN,null,COL_CREATEDON);
    $cols = array(COL_SKPDNAMA,COL_SKPDNAMAPIMPINAN);

    $queryAll = $this->db->get(TBL_MSKPD);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('mskpd.*, _userinformation.Name, _userinformation.Name')
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_MSKPD.".".COL_CREATEDON,"left")
    ->get_compiled_select(TBL_MSKPD, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start")->result_array();
    $data = [];

    foreach($rec as $r) {
      $data[] = array(
        '<a class="btn btn-xs btn-danger btn-action" href="'.site_url('site/skpd/delete/'.$r[COL_SKPDID]).'"><i class="far fa-times-circle"></i></a>&nbsp;'.
        '<a class="btn btn-xs btn-primary btn-form" href="'.site_url('site/skpd/edit/'.$r[COL_SKPDID]).'"><i class="far fa-pencil"></i></a>'
        /*'<a class="btn btn-xs btn-'.($r[COL_SKPDISAKTIF]==1?'warning':'info').' btn-action" href=""><i class="far fa-refresh"></i></a>'*/,
        $r[COL_SKPDNAMA],
        $r[COL_SKPDNAMAPIMPINAN],
        $r[COL_SKPDISAKTIF]==1?'<span class="badge badge-info">AKTIF</span>':'<span class="badge badge-warning">INAKTIF</span>',
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $dat = array(
        COL_SKPDNAMA=>$this->input->post(COL_SKPDNAMA),
        COL_SKPDNAMAPIMPINAN=>$this->input->post(COL_SKPDNAMA),
        COL_SKPDISAKTIF=>1,
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MSKPD, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $this->load->view('site/skpd/form');
    }
  }

  public function edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $dat = array(
        COL_SKPDNAMA=>$this->input->post(COL_SKPDNAMA),
        COL_SKPDNAMAPIMPINAN=>$this->input->post(COL_SKPDNAMA),

        COL_UPDATEDON=>date('Y-m-d H:i:s'),
        COL_UPDATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_SKPDID, $id)->update(TBL_MSKPD, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $rdata = $this->db->where(COL_SKPDID, $id)->get(TBL_MSKPD)->row_array();
      if(empty($rdata)) {
        show_error('DATA TIDAK DITEMUKAN!');
        exit();
      }

      $this->load->view('site/skpd/form', array('data'=>$rdata));
    }
  }

  public function delete($id) {
    $ruser = GetLoggedUser();

    $this->db->trans_begin();
    try {
      $res = $this->db->where(COL_SKPDID, $id)->delete(TBL_MSKPD);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      ShowJsonSuccess('HAPUS DATA BERHASIL.');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }
}
