<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <div class="card-body p-0">
            <form action="<?=current_url()?>" method="get">
              <table class="table table-bordered mb-0">
                <thead>
                  <tr>
                    <td>
                      <div class="form-group row">
                        <label class="control-label col-lg-2">SKPD :</label>
                        <div class="col-lg-10">
                          <?php
                          $ruser = GetLoggedUser();
                          if($ruser[COL_ROLEID] != ROLEADMIN) {
                            $rskpd = $this->db
                            ->where(COL_SKPDID, $ruser[COL_COMPANYID])
                            ->get(TBL_MSKPD)
                            ->row_array();
                            ?>
                            <input type="hidden" name="IdSkpd" value="<?=$ruser[COL_COMPANYID]?>" />
                            <input type="text" class="form-control" value="<?=!empty($rskpd)?$rskpd[COL_SKPDNAMA]:'-'?>" readonly />
                            <?php
                          } else {
                            ?>
                            <select class="form-control" name="IdSkpd" style="width: 100%">
                              <?=GetCombobox("select * from mskpd where SkpdIsAktif=1 order by SkpdNama", COL_SKPDID, COL_SKPDNAMA, $getSKPD, true, false, '-- SEMUA SKPD --')?>
                            </select>
                            <?php
                          }
                          ?>
                        </div>
                      </div>
                      <div class="form-group row mb-0">
                        <label class="control-label col-lg-2">TAHUN :</label>
                        <div class="col-lg-2">
                          <select class="form-control" name="Tahun" style="width: 100%">
                            <?php
                            for($i=date('Y')-5; $i<=date('Y')+5; $i++) {
                              ?>
                              <option value="<?=$i?>" <?=$getTahun==$i?'selected':''?>><?=$i?></option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                    </td>
                  </tr>
                </thead>
              </table>
            </form>

          </div>
        </div>
        <?php
        if(isset($res)) {
          ?>
          <div class="card">
            <div class="card-body p-0">
              <table class="table table-bordered w-100">
                <thead>
                  <tr>
                    <!--<th>SASARAN</th>-->
                    <th>IKU</th>
                    <th>ANGGARAN</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $sum = 0;
                  $no=1;
                  if(!empty($res)) {
                    foreach($res as $r) {
                      if(!empty($getSKPD)) {
                        $this->db->where(TBL_TKEGIATAN.'.'.COL_IDSKPD, $getSKPD);
                      }

                      $rdet = $this->db
                      ->join(TBL_TKEGIATAN,TBL_TKEGIATAN.'.'.COL_KEGID." = ".TBL_TKEGIATAN_RPJMD.".".COL_IDKEG,"left")
                      ->where(COL_IDRPJMD, $r['IndikatorId'])
                      ->get(TBL_TKEGIATAN_RPJMD)
                      ->result_array();
                      ?>
                      <tr>
                        <!--<td><?=$r[COL_SASARANNAMA]?></td>-->
                        <td class="font-weight-bold"><?=$r[COL_INDIKATORURAIAN]?></td>
                        <td class="text-right font-weight-bold" style="width: 10px; white-space: nowrap; vertical-align: middle"><?=number_format($r[COL_KEGANGGARAN])?></td>
                      </tr>
                      <?php
                      if(!empty($getSKPD)) {
                        foreach($rdet as $d) {
                          ?>
                          <tr class="font-italic">
                            <td class="text-sm" style="padding-left: 2.5rem !important"><?=$d[COL_KEGNAMA]?></td>
                            <td class="text-sm text-right font-weight-bold" style="width: 10px; white-space: nowrap; vertical-align: middle"><?=number_format($d[COL_KEGANGGARAN])?></td>
                          </tr>
                          <?php
                        }
                      }
                      $no++;
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="2" class="text-center font-italic">(KOSONG)</td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  $('select', $('form')).change(function(){
    $(this).closest('form').submit();
  });
});
</script>
