<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?=$title?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-outline card-secondary">
          <div class="card-body p-0">
            <form action="<?=current_url()?>" method="get">
              <table class="table table-bordered mb-0">
                <thead>
                  <tr>
                    <td>
                      <div class="form-group row">
                        <label class="control-label col-lg-2">SKPD :</label>
                        <div class="col-lg-10">
                          <?php
                          $ruser = GetLoggedUser();
                          if($ruser[COL_ROLEID] != ROLEADMIN) {
                            $rskpd = $this->db
                            ->where(COL_SKPDID, $ruser[COL_COMPANYID])
                            ->get(TBL_MSKPD)
                            ->row_array();
                            ?>
                            <input type="hidden" name="IdSkpd" value="<?=$ruser[COL_COMPANYID]?>" />
                            <input type="text" class="form-control" value="<?=!empty($rskpd)?$rskpd[COL_SKPDNAMA]:'-'?>" readonly />
                            <?php
                          } else {
                            ?>
                            <select class="form-control" name="IdSkpd" style="width: 100%">
                              <?=GetCombobox("select * from mskpd where SkpdIsAktif=1 order by SkpdNama", COL_SKPDID, COL_SKPDNAMA, $getSKPD, true, false, '-- SEMUA SKPD --')?>
                            </select>
                            <?php
                          }
                          ?>
                        </div>
                      </div>
                      <div class="form-group row mb-0">
                        <label class="control-label col-lg-2">TAHUN :</label>
                        <div class="col-lg-2">
                          <select class="form-control" name="Tahun" style="width: 100%">
                            <?php
                            for($i=date('Y')-5; $i<=date('Y')+5; $i++) {
                              ?>
                              <option value="<?=$i?>" <?=$getTahun==$i?'selected':''?>><?=$i?></option>
                              <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                    </td>
                  </tr>
                </thead>
              </table>
            </form>

          </div>
        </div>
        <?php
        if(isset($res)) {
          ?>
          <div class="card">
            <div class="card-body p-0">
              <table class="table table-bordered w-100">
                <thead>
                  <tr>
                    <?php
                    if(empty($getSKPD)) {
                      ?>
                      <th>SKPD</th>
                      <?php
                    }
                    ?>
                    <th>KEGIATAN / SUB KEGIATAN</th>
                    <th>SASARAN</th>
                    <th>ANGGARAN</th>
                    <th style="width: 10px; white-space: nowrap">SKOR</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $sum = 0;
                  $no=1;
                  if(!empty($res)) {
                    $max = $res[0]['Skor'];
                    $med = round((3/4)*$max, 0);
                    $mid = round((1/2)*$max, 0);
                    $low = round((1/4)*$max, 0);

                    foreach($res as $r) {
                      $_color = 'text-red';
                      if($r['Skor'] <= $med) $_color = 'text-orange';
                      else if($r['Skor'] <= $mid) $_color = 'text-yellow';
                      else if($r['Skor'] <= $low) $_color = 'text-green';
                      ?>
                      <tr class="<?=$_color?>">
                        <?php
                        if(empty($getSKPD)) {
                          ?>
                          <td><?=$r[COL_SKPDNAMA]?></td>
                          <?php
                        }
                        ?>
                        <td><?=$r[COL_KEGNAMA]?><br /><small class="font-italic"><?=$r[COL_KEGKODE]?></small></td>
                        <td><?=$r[COL_KEGSASARAN]?><br /><small class="font-italic"><?=$r[COL_KEGTARGET]?> (<?=$r[COL_KEGSATUAN]?>)</small></td>
                        <td class="text-right" style="width: 10px; white-space: nowrap"><?=number_format($r[COL_KEGANGGARAN])?></td>
                        <td class="text-right" style="width: 10px; white-space: nowrap"><?=number_format($r['Skor'])?></td>
                      </tr>
                      <?php
                      $no++;
                    }
                  } else {
                    ?>
                    <tr>
                      <td colspan="<?=empty($getSKPD)?'5':'4'?>" class="text-center font-italic">(KOSONG)</td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          <?php
        }
        ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  $('select', $('form')).change(function(){
    $(this).closest('form').submit();
  });
});
</script>
