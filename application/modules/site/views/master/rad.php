<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <div class="card-tools text-center" style="float: none !important">
              <a href="<?=site_url('site/master/rad-add')?>" type="button" class="btn btn-tool btn-add text-primary"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH</a>
            </div>
          </div>
          <div class="card-body p-0">
            <table id="datalist" class="table table-bordered table-condensed">
              <thead>
                <tr>
                  <th class="text-center" style="width: 10px">AKSI</th>
                  <th>JUDUL RAD</th>
                  <th>MASALAH</th>
                  <th>STRATEGI</th>
                  <th>DIINPUT PADA</th>
                </tr>
              </thead>
              <tbody>
                <?php
                foreach($res as $r) {
                  $rdet = $this->db
                  ->where(COL_IDRAD, $r[COL_UNIQ])
                  ->order_by(COL_RADINDIKATORNO)
                  ->get(TBL_MRAD_DET)
                  ->result_array();
                  ?>
                  <tr>
                    <td style="width: 10px; white-space: nowrap; vertical-align: middle" <?=count($rdet)>1?'rowspan='.count($rdet):''?>>
                      <a class="btn btn-xs btn-danger btn-action" href="<?=site_url('site/master/rad-delete/'.$r[COL_UNIQ])?>"><i class="far fa-times-circle"></i></a>&nbsp;
                      <a class="btn btn-xs btn-primary" href="<?=site_url('site/master/rad-edit/'.$r[COL_UNIQ])?>"><i class="far fa-search"></i></a>
                    </td>
                    <td class="font-weight-bold" style="vertical-align: middle" <?=count($rdet)>1?'rowspan='.count($rdet):''?>>
                      <?=$r[COL_RADJUDUL]?>
                    </td>
                    <?php
                    if(!empty($rdet)) {
                      ?>
                      <td class="text-sm font-italic" style="padding-left: .75rem !important"><?=$rdet[0][COL_RADINDIKATORMASALAH]?></td>
                      <td class="text-sm font-italic"><?=$rdet[0][COL_RADINDIKATORURAIAN]?></td>
                      <?php
                    } else {
                      ?>
                      <td class="text-sm font-italic" style="padding-left: .75rem !important">-</td>
                      <td class="text-sm font-italic">-</td>
                      <?php
                    }
                    ?>
                    <td style="width: 10px; white-space: nowrap; vertical-align: middle" <?=count($rdet)>1?'rowspan='.count($rdet):''?>>
                      <?=date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))?>
                    </td>
                  </tr>
                  <?php
                  if(!empty($rdet) && count($rdet)>1) {
                    for($i=1; $i<count($rdet); $i++) {
                      ?>
                      <tr>
                        <td class="text-sm font-italic" style="padding-left: .75rem !important"><?=$rdet[$i][COL_RADINDIKATORMASALAH]?></td>
                        <td class="text-sm font-italic"><?=$rdet[$i][COL_RADINDIKATORURAIAN]?></td>
                      </tr>
                      <?php
                    }
                  }
                }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-add" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title font-weight-bold">TAMBAH RAD</span>
      </div>
      <form id="form-rad" action="<?=current_url()?>" method="post" enctype="multipart/form-data">
        <div class="modal-body">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
          <button type="submit" id="btn-save" class="btn btn-sm btn-success"><i class="far fa-arrow-circle-right"></i>&nbsp;SIMPAN</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  $('.btn-action').click(function() {
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success(res.success);
        }
      }, "json").done(function() {
        location.reload();
      }).fail(function() {
        toastr.error('SERVER ERROR');
      });
    }
    return false;
  });

  $('.btn-add').click(function() {
    var url = $(this).attr('href');
    $('.modal-body', $('#modal-add')).empty();
    $('.modal-body', $('#modal-add')).load(url, function(){
      $('#form-rad', $('#modal-add')).attr('action', url);
      $("select", $('#modal-add')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('#modal-add').modal('show');
    });
    return false;
  });

  $('#form-rad').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      //btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            location.reload();
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          //btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
