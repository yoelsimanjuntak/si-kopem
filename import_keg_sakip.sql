select 
#*,
'NULL' as KegId,
skpd.SkpdId as IdSkpd,
skpd.SkpdNama as SKPD,
'2022' as KegTahun,
UPPER(REPLACE(subkeg.SubkegUraian,'\n',' ')) as KegNama,
mkeg.KegUrusan,
mkeg.KegKode,
mkeg.KegSasaran,
mkeg.KegIndikator,
(select kegsas.SasaranSatuan from sakipv2_subbid_subkegsasaran kegsas where kegsas.IdSubkeg = subkeg.SubkegId limit 1) as KegSatuan, 
(select kegsas.SasaranTarget from sakipv2_subbid_subkegsasaran kegsas where kegsas.IdSubkeg = subkeg.SubkegId limit 1) as KegTarget, 
subkeg.SubkegPagu as KegAnggaran,
'admin' as CreatedBy,
'2022-10-28 12:51:00' as CreatedOn,
'NULL' as UpdatedBy,
'NULL' as UpdatedOn
#skpd.SkpdNama,
#ren.RenstraUraian,
#subkeg.SubkegKode,

#mkeg2.KegKode,
from sakipv2_subbid_subkegiatan subkeg
left join sakipv2_subbid sb on sb.SubbidId = subkeg.IdSubbid
left join sakipv2_bid bid on bid.BidId = sb.IdBid
left join sakipv2_skpd_renstra ren on ren.RenstraId = bid.IdRenstra
left join sakipv2_skpd skpd on skpd.SkpdId = ren.IdSkpd
left join sakipv2_bid_kegiatan keg on keg.KegiatanId = subkeg.IdKegiatan
left join sakipv2_bid_program prog on prog.ProgramId = keg.IdProgram
left join sakipv2_skpd_renstra_dpa dpa on dpa.DPAId = prog.IdDPA
left join mkegiatan mkeg on UPPER(mkeg.KegNama) = UPPER(REPLACE(subkeg.SubkegUraian,'\n',' '))
#left join mkegiatan mkeg2 on mkeg2.KegKode = REPLACE(subkeg.SubkegKode,' ','.')
where
	dpa.DPATahun = 2022
	/*and mkeg.KegKode is null
	and mkeg2.KegKode is null*/
	and (mkeg.KegKode is not null/* or mkeg2.KegKode is not null*/)
order by skpd.SkpdId