<?php
$ruser = GetLoggedUser();
$arrIKU = array();
$arrRAD = array();
if(!empty($data)) {
  $detIKU = $this->db
  ->select('tkegiatan_rpjmd.*, mrpjmd_det.IndikatorUraian')
  ->join(TBL_MRPJMD_DET,TBL_MRPJMD_DET.'.'.COL_UNIQ." = ".TBL_TKEGIATAN_RPJMD.".".COL_IDRPJMD,"left")
  ->where(COL_IDKEG, $data[COL_KEGID])
  ->get(TBL_TKEGIATAN_RPJMD)
  ->result_array();

  $detRAD = $this->db
  ->select('tkegiatan_rad.*, mrad_det.RADIndikatorUraian')
  ->join(TBL_MRAD_DET,TBL_MRAD_DET.'.'.COL_UNIQ." = ".TBL_TKEGIATAN_RAD.".".COL_IDRAD,"left")
  ->where(COL_IDKEG, $data[COL_KEGID])
  ->get(TBL_TKEGIATAN_RAD)
  ->result_array();

  foreach($detIKU as $d) {
    $arrIKU[] = array(
      'Uniq'=>$d[COL_IDRPJMD],
      'IndikatorUraian'=>$d[COL_INDIKATORURAIAN],
      'IndikatorLevel'=>$d[COL_LEVEL]
    );
  }

  foreach($detRAD as $d) {
    $arrRAD[] = array(
      'Uniq'=>$d[COL_IDRAD],
      'RADIndikatorUraian'=>$d[COL_RADINDIKATORURAIAN],
      'RADIndikatorLevel'=>$d[COL_LEVEL]
    );
  }
}
?>
<?=form_open_multipart(current_url(), array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 text-right">
        <a href="<?=site_url('site/kegiatan/index')?>" class="btn btn-secondary btn-sm"><i class="far fa-arrow-circle-left"></i>&nbsp;KEMBALI</a>&nbsp;
        <button type="submit" class="btn btn-primary btn-sm"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row pl-2 pr-2">
      <div class="col-sm-6">
        <div class="card card-outline card-primary">
          <div class="card-header">
            <h6 class="card-title font-weight-bold">RINCIAN</h6>
          </div>

          <input type="hidden" name="KegUniq" />
          <input type="hidden" name="KegIKU" />
          <input type="hidden" name="KegRAD" />
          <div class="card-body">
            <?php
            if($ruser[COL_ROLEID]==ROLEADMIN) {
              ?>
              <div class="form-group">
                <label>SKPD</label>
                <select class="form-control" name="<?=COL_IDSKPD?>" style="width: 100%" required>
                  <?=GetCombobox("select * from mskpd where SkpdIsAktif=1 order by SkpdNama", COL_SKPDID, COL_SKPDNAMA, (!empty($data)?$data[COL_IDSKPD]:''), true, false, '-- PILIH SKPD --')?>
                </select>
              </div>
              <?php
            } else {
              ?>
              <input type="hidden" name="<?=COL_IDSKPD?>" value="<?=$ruser[COL_COMPANYID]?>" />
              <?php
            }
            ?>
            <div class="form-group">
              <label>URUSAN</label>
              <select class="form-control" name="<?=COL_KEGURUSAN?>" style="width: 100%" required>
                <?=GetCombobox("select mkegiatan.KegUrusan from mkegiatan group by KegUrusan order by KegUrusan", COL_KEGURUSAN, COL_KEGURUSAN, (!empty($data)?$data[COL_KEGURUSAN]:''), true, false, '-- PILIH URUSAN --')?>
              </select>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <label>KEGIATAN</label>
                  <div class="input-group">
                    <input type="text" class="form-control" name="<?=COL_KEGKODE?>" value="<?=!empty($data)?$data[COL_KEGKODE]:''?>" readonly required>
                    <span class="input-group-append">
                      <button type="button" id="btn-browse-kegiatan" class="btn btn-primary btn-sm"><i class="far fa-search"></i>&nbsp;PILIH</button>
                    </span>
                  </div>
                </div>
                <div class="col-sm-6">
                  <label>TAHUN</label>
                  <input type="text" class="form-control" name="<?=COL_KEGTAHUN?>" value="<?=!empty($data)?$data[COL_KEGTAHUN]:date('Y')?>">
                </div>
              </div>

            </div>
            <div class="form-group">
              <label>URAIAN</label>
              <textarea class="form-control" name="<?=COL_KEGNAMA?>" readonly required><?=!empty($data)?$data[COL_KEGNAMA]:''?></textarea>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <label>SASARAN</label>
                  <textarea class="form-control" rows="4" name="<?=COL_KEGSASARAN?>" readonly><?=!empty($data)?$data[COL_KEGSASARAN]:''?></textarea>
                </div>
                <div class="col-sm-6">
                  <label>INDIKATOR</label>
                  <textarea class="form-control" rows="4" name="<?=COL_KEGINDIKATOR?>" readonly><?=!empty($data)?$data[COL_KEGINDIKATOR]:''?></textarea>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <label>SATUAN</label>
                  <input type="text" class="form-control" name="<?=COL_KEGSATUAN?>" value="<?=!empty($data)?$data[COL_KEGSATUAN]:''?>" readonly>
                </div>
                <div class="col-sm-6">
                  <label>TARGET</label>
                  <input type="text" class="form-control" name="<?=COL_KEGTARGET?>" value="<?=!empty($data)?$data[COL_KEGTARGET]:''?>" required>
                </div>
              </div>
            </div>
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <label>ANGGARAN</label>
                  <input type="text" class="form-control uang text-right" name="<?=COL_KEGANGGARAN?>" value="<?=!empty($data)?$data[COL_KEGANGGARAN]:''?>"  required />
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card  card-outline card-primary">
          <div class="card-header">
            <h6 class="card-title font-weight-bold">KETERKAITAN IKU</h6>
          </div>
          <div class="card-body p-0">
            <table id="det-iku" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>NO.</th>
                  <th>IKU</th>
                  <th style="width: 100px; white-space: nowrap">SKALA</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
          <div class="card-footer">
            <button type="button" id="btn-add-det-iku" class="btn btn-primary btn-sm"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH</button>
          </div>
        </div>
        <div class="card  card-outline card-primary">
          <div class="card-header">
            <h6 class="card-title font-weight-bold">KETERKAITAN RAD</h6>
          </div>
          <div class="card-body p-0">
            <table id="det-rad" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th>NO.</th>
                  <th>Strategi RAD</th>
                  <th style="width: 100px; white-space: nowrap">SKALA</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
          <div class="card-footer">
            <button type="button" id="btn-add-det-rad" class="btn btn-primary btn-sm"><i class="far fa-plus-circle"></i>&nbsp;TAMBAH</button>
          </div>
        </div>
    </div>
  </div>
</section>
<?=form_close()?>
<div class="modal fade" id="modal-browse" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title font-weight-bold">PILIH KEGIATAN</span>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-browse-iku" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title font-weight-bold">PILIH IKU</span>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>SASARAN</label>
          <select class="form-control" name="BrowseIKU" style="width: 100%" required>
            <?=GetCombobox("select * from mrpjmd order by Uniq", COL_UNIQ, COL_SASARANNAMA, null, true, false, '-- PILIH SASARAN --')?>
          </select>
        </div>
        <div class="form-group">
          <table id="browse-iku" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th style="width: 10px; white-space: nowrap">No.</th>
                <th>Indikator</th>
                <th>Satuan</th>
                <th style="width: 150px; white-space: nowrap">Target</th>
                <th class="text-center" style="width: 10px; white-space: nowrap">#</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-browse-rad" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title font-weight-bold">PILIH INDIKATOR RAD</span>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>RAD</label>
          <select class="form-control" name="BrowseRAD" style="width: 100%" required>
            <?=GetCombobox("select * from mrad order by Uniq", COL_UNIQ, COL_RADJUDUL, null, true, false, '-- PILIH RAD --')?>
          </select>
        </div>
        <div class="form-group">
          <table id="browse-rad" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th style="width: 10px; white-space: nowrap">No.</th>
                <th>Strategi</th>
                <!--<th>Satuan</th>
                <th style="width: 150px; white-space: nowrap">Target</th>-->
                <th class="text-center" style="width: 10px; white-space: nowrap">#</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  var modBrowse = $('#modal-browse');
  var modBrowseIKU = $('#modal-browse-iku');
  var modBrowseRAD = $('#modal-browse-rad');

  $('[name=KegUrusan]').change(function(){
    $('[name=KegUniq]').val('').trigger('change');
  });

  $('#btn-browse-kegiatan').click(function(){
    var _urusan = $('[name=KegUrusan]').val();
    if(!_urusan) {
      alert('Silakan pilih URUSAN terlebih dahulu!');
      return false;
    }

    $('.modal-body', modBrowse).html('<p class="font-italic text-center">MEMUAT...</p>');
    $('.modal-body', modBrowse).load('<?=site_url('site/kegiatan/browse')?>', {Urusan: _urusan}, function(){
      $('#modal-browse').modal('show');
    });

  });

  $('[name=KegUniq]').change(function(){
    var _uniq = $(this).val();
    $.get('<?=site_url('site/kegiatan/get')?>', {Uniq: _uniq}, function(dat) {
      dat = JSON.parse(dat);
      if(dat) {
        $('[name=KegKode]').val(dat.KegKode);
        $('[name=KegNama]').val(dat.KegNama);
        $('[name=KegSasaran]').val(dat.KegSasaran);
        $('[name=KegIndikator]').val(dat.KegIndikator);
        $('[name=KegSatuan]').val(dat.KegSatuan);
      } else {
        $('[name=KegKode]').val('');
        $('[name=KegNama]').val('');
        $('[name=KegSasaran]').val('');
        $('[name=KegIndikator]').val('');
        $('[name=KegSatuan]').val('');
      }
      modBrowse.modal('hide');
    });
  });

  $('#btn-add-det-iku').click(function(){
    modBrowseIKU.modal('show');
  });
  $('#btn-add-det-rad').click(function(){
    modBrowseRAD.modal('show');
  });

  $('[name=BrowseIKU]').change(function(){
    var _uniq = $(this).val();
    $('table#browse-iku>tbody', modBrowseIKU).load('<?=site_url('site/kegiatan/browse-iku')?>', {IdRPJMD: _uniq}, function(){
      $('.btn-select', $('table#browse-iku>tbody', modBrowseIKU)).click(function(){
        var _uniq = $(this).data('uniq');
        var _uraian = $(this).data('uraian');

        var _iku = $('[name=KegIKU]').val();
        var arrIKU = [];
        if(_iku) {
          arrIKU = JSON.parse(decodeURIComponent(_iku));
        }

        var _exist = $.grep(arrIKU, function (d, i) {
          return d.Uniq == _uniq;
        });

        if(_exist.length==0) {
          arrIKU.push({Uniq: _uniq, IndikatorUraian: _uraian, IndikatorLevel: 'NORMAL'});
        }

        $('[name=KegIKU]').val(JSON.stringify(arrIKU)).trigger('change');
        modBrowseIKU.modal('hide');
      });
    });
  }).trigger('change');

  $('[name=BrowseRAD]').change(function(){
    var _uniq = $(this).val();
    $('table#browse-rad>tbody', modBrowseRAD).load('<?=site_url('site/kegiatan/browse-rad')?>', {IdRAD: _uniq}, function(){
      $('.btn-select', $('table#browse-rad>tbody', modBrowseRAD)).click(function(){
        var _uniq = $(this).data('uniq');
        var _uraian = $(this).data('uraian');

        var _iku = $('[name=KegRAD]').val();
        var arrIKU = [];
        if(_iku) {
          arrIKU = JSON.parse(decodeURIComponent(_iku));
        }

        var _exist = $.grep(arrIKU, function (d, i) {
          return d.Uniq == _uniq;
        });

        if(_exist.length==0) {
          arrIKU.push({Uniq: _uniq, RADIndikatorUraian: _uraian, RADIndikatorLevel: 'NORMAL'});
        }

        $('[name=KegRAD]').val(JSON.stringify(arrIKU)).trigger('change');
        modBrowseRAD.modal('hide');
      });
    });
  }).trigger('change');

  $('[name=KegIKU]').change(function(){
    var arrIKU = [];
    var _iku = $('[name=KegIKU]').val();

    if(_iku) {
      arrIKU = JSON.parse(decodeURIComponent(_iku));
    }
    var _html = '';
    if(arrIKU.length > 0) {
      for(var i=0; i<arrIKU.length; i++) {
        _html += '<tr>';
        _html += '<td style="width: 10px; white-space: nowrap">'+(i+1)+'</td>';
        _html += '<td>'+arrIKU[i].IndikatorUraian+'</td>';
        <?php
        if($ruser[COL_ROLEID]==ROLEADMIN) {
          ?>
          _html += '<td class="text-right" style="width: 100px; white-space: nowrap">'+
                      '<select class="no-select2 select-det-level" style="width: 100px" data-uniq="'+arrIKU[i].Uniq+'">'+
                        '<option value="TINGGI" '+(arrIKU[i].IndikatorLevel=='TINGGI'?'selected':'')+'>TINGGI</option>'+
                        '<option value="NORMAL" '+(arrIKU[i].IndikatorLevel=='NORMAL'?'selected':'')+'>NORMAL</option>'+
                        '<option value="RENDAH" '+(arrIKU[i].IndikatorLevel=='RENDAH'?'selected':'')+'>RENDAH</option>'+
                      '</select>'+
                    '</td>';
          <?php
        } else {
          ?>
          _html += '<td class="font-italic">'+arrIKU[i].IndikatorLevel+'</td>';
          <?php
        }
        ?>

        _html += '<td style="width: 10px; white-space: nowrap" class="text-center"><button type="button" class="btn btn-danger btn-xs btn-del-det" data-uniq="'+arrIKU[i].Uniq+'"><i class="far fa-times-circle"></i></button></td>';
        _html += '</tr>';
      }
    } else {
      _html += '<tr><td colspan="3" class="text-center font-italic">BELUM ADA DATA</td></tr>';
    }

    $('#det-iku>tbody').html(_html);

    $('.select-det-level', $('#det-iku')).change(function() {
      var _uniq = $(this).data('uniq');
      var _val = $(this).val();
      if(_uniq) {
        var _arr = $('[name=KegIKU]').val();
        _arr = JSON.parse(decodeURIComponent(_arr));

        var _arrNew = $.map(_arr, function(n) {
          if(n.Uniq == _uniq) {
            return {Uniq: n.Uniq, IndikatorUraian: n.IndikatorUraian, IndikatorLevel: _val};
          } else {
            return n;
          }
        });
        $('[name=KegIKU]').val(JSON.stringify(_arrNew)).trigger('change');
      }
    });

    $('.btn-del-det', $('#det-iku')).click(function() {
      var _uniq = $(this).data('uniq');
      if(_uniq) {
        var _arr = $('[name=KegIKU]').val();
        _arr = JSON.parse(decodeURIComponent(_arr));

        var _arrNew = $.grep(_arr, function(e){ return e.Uniq != _uniq; });
        $('[name=KegIKU]').val(JSON.stringify(_arrNew)).trigger('change');
      }
    });
  }).val('<?=!empty($arrIKU)?str_replace('\t', '', str_replace('\n', '', preg_replace("/\s+/", " ", json_encode($arrIKU)))):'[]'?>').trigger('change');

  $('[name=KegRAD]').change(function(){
    var arrIKU = [];
    var _iku = $('[name=KegRAD]').val();

    if(_iku) {
      arrIKU = JSON.parse(decodeURIComponent(_iku));
    }
    var _html = '';
    if(arrIKU.length > 0) {
      for(var i=0; i<arrIKU.length; i++) {
        _html += '<tr>';
        _html += '<td style="width: 10px; white-space: nowrap">'+(i+1)+'</td>';
        _html += '<td>'+arrIKU[i].RADIndikatorUraian+'</td>';
        <?php
        if($ruser[COL_ROLEID]==ROLEADMIN) {
          ?>
          _html += '<td class="text-right" style="width: 100px; white-space: nowrap">'+
                      '<select class="no-select2 select-det-level" style="width: 100px" data-uniq="'+arrIKU[i].Uniq+'">'+
                        '<option value="TINGGI" '+(arrIKU[i].RADIndikatorLevel=='TINGGI'?'selected':'')+'>TINGGI</option>'+
                        '<option value="NORMAL" '+(arrIKU[i].RADIndikatorLevel=='NORMAL'?'selected':'')+'>NORMAL</option>'+
                      '</select>'+
                    '</td>';
          <?php
        } else {
          ?>
          _html += '<td class="font-italic">'+arrIKU[i].RADIndikatorLevel+'</td>';
          <?php
        }
        ?>
        _html += '<td style="width: 10px; white-space: nowrap" class="text-center"><button type="button" class="btn btn-danger btn-xs btn-del-det" data-uniq="'+arrIKU[i].Uniq+'"><i class="far fa-times-circle"></i></button></td>';
        _html += '</tr>';
      }
    } else {
      _html += '<tr><td colspan="3" class="text-center font-italic">BELUM ADA DATA</td></tr>';
    }

    $('#det-rad>tbody').html(_html);

    $('.select-det-level', $('#det-rad')).change(function() {
      var _uniq = $(this).data('uniq');
      var _val = $(this).val();
      if(_uniq) {
        var _arr = $('[name=KegRAD]').val();
        _arr = JSON.parse(decodeURIComponent(_arr));

        var _arrNew = $.map(_arr, function(n) {
          if(n.Uniq == _uniq) {
            return {Uniq: n.Uniq, RADIndikatorUraian: n.RADIndikatorUraian, RADIndikatorLevel: _val};
          } else {
            return n;
          }
        });
        $('[name=KegRAD]').val(JSON.stringify(_arrNew)).trigger('change');
      }
    });
    $('.btn-del-det', $('#det-rad')).click(function() {
      var _uniq = $(this).data('uniq');
      if(_uniq) {
        var _arr = $('[name=KegRAD]').val();
        _arr = JSON.parse(decodeURIComponent(_arr));

        var _arrNew = $.grep(_arr, function(e){ return e.Uniq != _uniq; });
        $('[name=KegRAD]').val(JSON.stringify(_arrNew)).trigger('change');
      }
    });
  }).val('<?=!empty($arrRAD)?str_replace('\t', '', str_replace('\n', '', preg_replace("/\s+/", " ", json_encode($arrRAD)))):'[]'?>').trigger('change');


  $('#form-main').validate({
    ignore: "input[type='file']",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.redirect) {
              location.href = res.redirect;
            } else {
              setTimeout(function(){
                location.reload();
              }, 1000);
            }
          }
        },
        error: function(data) {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
