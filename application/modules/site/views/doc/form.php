<?php
$ruser = GetLoggedUser();
if($ruser[COL_ROLEID]==ROLEADMIN) {
  ?>
  <div class="form-group">
    <label>SKPD</label>
    <select class="form-control" name="<?=COL_IDSKPD?>" style="width: 100%">
      <?=GetCombobox("select * from mskpd where SkpdIsAktif=1 order by SkpdNama", COL_SKPDID, COL_SKPDNAMA, null, true, false, '-- PILIH SKPD --')?>
    </select>
  </div>
  <?php
}
?>

<div class="form-group">
  <label>NAMA DOKUMEN</label>
  <input type="text" class="form-control" placeholder="Nama / Judul Dokumen" name="<?=COL_DOCNAME?>" value="<?=!empty($data)?$data[COL_DOCNAME]:''?>" />
</div>
<div class="form-group">
  <div class="row">
    <div class="col-sm-9">
      <label>NOMOR</label>
      <input type="text" class="form-control" placeholder="Nomor Dokumen" name="<?=COL_DOCNOMOR?>" value="<?=!empty($data)?$data[COL_DOCNOMOR]:''?>" />
    </div>
    <div class="col-sm-3">
      <label>TAHUN <small>(OPSIONAL)</small></label>
      <input type="number" class="form-control" placeholder="Tahun" name="<?=COL_DOCTAHUN?>" value="<?=!empty($data)?$data[COL_DOCTAHUN]:''?>" />
    </div>
  </div>
</div>
<div class="form-group">
  <label>KETERANGAN</label>
  <textarea class="form-control" rows="2" placeholder="Keterangan / Rincian / Catatan Tambahan" name="<?=COL_DOCREMARKS?>"><?=!empty($data)?$data[COL_DOCREMARKS]:''?></textarea>
</div>
<div class="form-group">
  <label>UNGGAH</label>
  <div id="div-attachment">
    <div class="input-group mb-2">
      <div class="input-group-prepend">
        <span class="input-group-text"><i class="fad fa-paperclip"></i></span>
      </div>
      <div class="custom-file">
        <input type="file" class="custom-file-input" name="file" accept="image/*,application/pdf,application/vnd.ms-excel">
        <label class="custom-file-label" for="file">PILIH FILE</label>
      </div>
    </div>
    <p class="text-sm text-muted mb-0 font-italic">
      <strong>CATATAN:</strong><br />
      - Besar file / dokumen maksimum <strong>5 MB</strong><br />
      - Jenis file / dokumen yang diperbolehkan hanya dalam format <strong>JPG / JPEG</strong>, <strong>PNG</strong>, <strong>PDF</strong>, <strong>DOC / DOCX</strong>, <strong>XLS / XLSX</strong>
    </p>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  bsCustomFileInput.init();
});
</script>
