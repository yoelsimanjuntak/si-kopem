<?php
define('TBL__HOMEPAGE','_homepage');
define('TBL__LOGS','_logs');
define('TBL__POSTCATEGORIES','_postcategories');
define('TBL__POSTIMAGES','_postimages');
define('TBL__POSTS','_posts');
define('TBL__ROLES','_roles');
define('TBL__SETTINGS','_settings');
define('TBL__USERINFORMATION','_userinformation');
define('TBL__USERS','_users');
define('TBL_MKEGIATAN','mkegiatan');
define('TBL_MRAD','mrad');
define('TBL_MRAD_DET','mrad_det');
define('TBL_MRPJMD','mrpjmd');
define('TBL_MRPJMD_DET','mrpjmd_det');
define('TBL_MSKPD','mskpd');
define('TBL_TDOCLOGS','tdoclogs');
define('TBL_TDOCS','tdocs');
define('TBL_TKEGIATAN','tkegiatan');
define('TBL_TKEGIATAN_RAD','tkegiatan_rad');
define('TBL_TKEGIATAN_RPJMD','tkegiatan_rpjmd');

define('COL_UNIQ','Uniq');
define('COL_CONTENTID','ContentID');
define('COL_CONTENTTITLE','ContentTitle');
define('COL_CONTENTTYPE','ContentType');
define('COL_CONTENTDESC1','ContentDesc1');
define('COL_CONTENTDESC2','ContentDesc2');
define('COL_CONTENTDESC3','ContentDesc3');
define('COL_CONTENTDESC4','ContentDesc4');
define('COL_TIMESTAMP','Timestamp');
define('COL_URL','URL');
define('COL_CLIENTINFO','ClientInfo');
define('COL_POSTCATEGORYID','PostCategoryID');
define('COL_POSTCATEGORYNAME','PostCategoryName');
define('COL_POSTCATEGORYLABEL','PostCategoryLabel');
define('COL_ISSHOWEDITOR','IsShowEditor');
define('COL_ISALLOWEXTERNALURL','IsAllowExternalURL');
define('COL_ISDOCUMENTONLY','IsDocumentOnly');
define('COL_POSTIMAGEID','PostImageID');
define('COL_POSTID','PostID');
define('COL_IMGPATH','ImgPath');
define('COL_IMGDESC','ImgDesc');
define('COL_IMGSHORTCODE','ImgShortcode');
define('COL_ISHEADER','IsHeader');
define('COL_ISTHUMBNAIL','IsThumbnail');
define('COL_DESCRIPTION','Description');
define('COL_POSTUNITID','PostUnitID');
define('COL_POSTDATE','PostDate');
define('COL_POSTTITLE','PostTitle');
define('COL_POSTSLUG','PostSlug');
define('COL_POSTCONTENT','PostContent');
define('COL_POSTEXPIREDDATE','PostExpiredDate');
define('COL_POSTMETATAGS','PostMetaTags');
define('COL_ISRUNNINGTEXT','IsRunningText');
define('COL_TOTALVIEW','TotalView');
define('COL_LASTVIEWDATE','LastViewDate');
define('COL_ISSUSPEND','IsSuspend');
define('COL_FILENAME','FileName');
define('COL_CREATEDBY','CreatedBy');
define('COL_CREATEDON','CreatedOn');
define('COL_UPDATEDBY','UpdatedBy');
define('COL_UPDATEDON','UpdatedOn');
define('COL_ROLEID','RoleID');
define('COL_ROLENAME','RoleName');
define('COL_SETTINGID','SettingID');
define('COL_SETTINGLABEL','SettingLabel');
define('COL_SETTINGNAME','SettingName');
define('COL_SETTINGVALUE','SettingValue');
define('COL_USERNAME','UserName');
define('COL_EMAIL','Email');
define('COL_COMPANYID','CompanyID');
define('COL_NAME','Name');
define('COL_IDENTITYNO','IdentityNo');
define('COL_BIRTHDATE','BirthDate');
define('COL_RELIGIONID','ReligionID');
define('COL_GENDER','Gender');
define('COL_ADDRESS','Address');
define('COL_PHONENUMBER','PhoneNumber');
define('COL_EDUCATIONID','EducationID');
define('COL_UNIVERSITYNAME','UniversityName');
define('COL_FACULTYNAME','FacultyName');
define('COL_MAJORNAME','MajorName');
define('COL_ISGRADUATED','IsGraduated');
define('COL_GRADUATEDDATE','GraduatedDate');
define('COL_YEAROFEXPERIENCE','YearOfExperience');
define('COL_RECENTPOSITION','RecentPosition');
define('COL_RECENTSALARY','RecentSalary');
define('COL_EXPECTEDSALARY','ExpectedSalary');
define('COL_CVFILENAME','CVFilename');
define('COL_IMAGEFILENAME','ImageFilename');
define('COL_REGISTEREDDATE','RegisteredDate');
define('COL_PASSWORD','Password');
define('COL_LASTLOGIN','LastLogin');
define('COL_LASTLOGINIP','LastLoginIP');
define('COL_KEGURUSAN','KegUrusan');
define('COL_KEGKODE','KegKode');
define('COL_KEGNAMA','KegNama');
define('COL_KEGSASARAN','KegSasaran');
define('COL_KEGINDIKATOR','KegIndikator');
define('COL_KEGSATUAN','KegSatuan');
define('COL_RADJUDUL','RADJudul');
define('COL_RADRUMUSAN','RADRumusan');
define('COL_RADSTRATEGI','RADStrategi');
define('COL_RADFILE','RADFile');
define('COL_IDRAD','IdRAD');
define('COL_RADINDIKATORNO','RADIndikatorNo');
define('COL_RADINDIKATORURAIAN','RADIndikatorUraian');
define('COL_RADINDIKATORMASALAH','RADIndikatorMasalah');
define('COL_RADSATUAN','RADSatuan');
define('COL_RADTARGET','RADTarget');
define('COL_RADSKOR','RADSkor');
define('COL_SASARANNAMA','SasaranNama');
define('COL_IDRPJMD','IdRPJMD');
define('COL_INDIKATORNO','IndikatorNo');
define('COL_INDIKATORURAIAN','IndikatorUraian');
define('COL_INDIKATORSATUAN','IndikatorSatuan');
define('COL_INDIKATORTARGET','IndikatorTarget');
define('COL_INDIKATORSKOR','IndikatorSkor');
define('COL_SKPDID','SkpdId');
define('COL_SKPDNAMA','SkpdNama');
define('COL_SKPDNAMAPIMPINAN','SkpdNamaPimpinan');
define('COL_SKPDNAMAJABATAN','SkpdNamaJabatan');
define('COL_SKPDKOP','SkpdKop');
define('COL_SKPDISAKTIF','SkpdIsAktif');
define('COL_DOCID','DocID');
define('COL_LOGREMARKS','LogRemarks');
define('COL_LOGTIMESTAMP','LogTimestamp');
define('COL_LOGFILE','LogFile');
define('COL_LOGBY','LogBy');
define('COL_IDSKPD','IdSkpd');
define('COL_DOCNAME','DocName');
define('COL_DOCREMARKS','DocRemarks');
define('COL_DOCNOMOR','DocNomor');
define('COL_DOCTAHUN','DocTahun');
define('COL_DOCURL','DocURL');
define('COL_DOCTYPE','DocType');
define('COL_DOCSTATUS','DocStatus');
define('COL_VERIFIEDBY','VerifiedBy');
define('COL_VERIFIEDON','VerifiedOn');
define('COL_ISVERIFKESESUAIAN','IsVerifKesesuaian');
define('COL_ISVERIFKODE','IsVerifKode');
define('COL_ISVERIFKELENGKAPAN','IsVerifKelengkapan');
define('COL_VERIFIEDREMARKS','VerifiedRemarks');
define('COL_KEGID','KegID');
define('COL_KEGTAHUN','KegTahun');
define('COL_KEGTARGET','KegTarget');
define('COL_KEGANGGARAN','KegAnggaran');
define('COL_IDKEG','IdKeg');
define('COL_BOBOT','Bobot');
define('COL_LEVEL','Level');
