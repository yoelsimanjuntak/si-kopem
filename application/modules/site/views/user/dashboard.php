<?php
$this->load->model('mpost');
$ruser = GetLoggedUser();
$docs = $this->mpost->search(0,"",3);
 ?>
 <style>
 th {
   border-right-width: 1px !important;
 }
 .table thead tr:first-child td {
   border-bottom: none !important;
}
 </style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?=$title?></h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <?php
      if($ruser[COL_ROLEID]==ROLEADMIN) {
        $_iku = $this->db->count_all_results(TBL_MRPJMD_DET);
        $_rad = $this->db->count_all_results(TBL_MRAD_DET);
        $_keg = $this->db->where(COL_KEGTAHUN, date('Y'))->count_all_results(TBL_TKEGIATAN);

        $rrekap = $this->db
        ->select('mskpd.SkpdNama, sum(KegAnggaran) as KegAnggaran, count(KegNama) as JlhKeg')
        ->join(TBL_MSKPD,TBL_MSKPD.'.'.COL_SKPDID." = ".TBL_TKEGIATAN.".".COL_IDSKPD,"left")
        ->where(COL_KEGTAHUN, date('Y'))
        ->group_by('mskpd.SkpdNama')
        ->get(TBL_TKEGIATAN)
        ->result_array();
        ?>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h3><?=number_format($_iku)?></h3>

              <p class="font-italic">INDIKATOR KINERJA UTAMA (IKU)</p>
            </div>
            <div class="icon">
              <i class="fas fa-bullseye-arrow"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-12">
          <div class="small-box bg-primary disabled">
            <div class="inner">
              <h3><?=number_format($_rad)?></h3>

              <p class="font-italic">INDIKATOR RAD</p>
            </div>
            <div class="icon">
              <i class="fas fa-books"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-4 col-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h3><?=number_format($_keg)?></h3>

              <p class="font-italic">KEGIATAN TA. <?=date('Y')?></p>
            </div>
            <div class="icon">
              <i class="fas fa-cogs"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
              <h5 class="card-title">REKAPITULASI KEGIATAN</h5>
            </div>
            <div class="card-body p-0">
              <table class="table table-bordered table-hover table-condensed" style="margin-top: 0 !important">
                <thead>
                  <tr>
                    <th style="white-space: nowrap">SKPD</th>
                    <th>JLH. KEGIATAN</th>
                    <th>JLH. ANGGARAN</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach($rrekap as $r) {
                    ?>
                    <tr>
                      <td style="white-space: nowrap"><?=$r[COL_SKPDNAMA]?></td>
                      <td class="text-right"><?=number_format($r['JlhKeg'])?></td>
                      <td class="text-right"><?=number_format($r['KegAnggaran'])?></td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <?php
      } else {
        $_keg = $this->db
        ->where(COL_IDSKPD, $ruser[COL_COMPANYID])
        ->where(COL_KEGTAHUN, date('Y'))
        ->count_all_results(TBL_TKEGIATAN);

        $_sum = $this->db
        ->select('sum(KegAnggaran) as KegAnggaran')
        ->where(COL_IDSKPD, $ruser[COL_COMPANYID])
        ->where(COL_KEGTAHUN, date('Y'))
        ->get(TBL_TKEGIATAN)
        ->row_array();
        ?>
        <div class="col-lg-6 col-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h3><?=number_format($_keg)?></h3>

              <p class="font-italic">KEGIATAN TA. <?=date('Y')?></p>
            </div>
            <div class="icon">
              <i class="fas fa-cogs"></i>
            </div>
          </div>
        </div>

        <div class="col-lg-6 col-12">
          <div class="small-box bg-primary">
            <div class="inner">
              <h3><?=number_format($_sum['KegAnggaran'])?></h3>

              <p class="font-italic">ANGGARAN TA. <?=date('Y')?></p>
            </div>
            <div class="icon">
              <i class="fas fa-sack-dollar"></i>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</section>
<script type="text/javascript">
</script>
