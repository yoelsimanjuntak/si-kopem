<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?= $title ?> <small class="text-sm font-weight-light"> Form</small></h1>
      </div><!-- /.col -->
      <!--<div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('user/index')?>"> Pengguna</a></li>
          <li class="breadcrumb-item active"><?=$edit?'Ubah':'Tambah'?></li>
        </ol>
      </div>-->
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-primary">
          <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6 pr-3">
                  <div class="form-group row">
                    <div class="col-sm-6">
                      <label>USERNAME</label>
                      <input type="text" class="form-control" name="<?=COL_USERNAME?>" value="<?= $edit ? $data[COL_USERNAME] : ""?>" <?=$edit?"disabled":""?> placeholder="Username" required>
                    </div>
                    <div class="col-sm-6">
                      <label>EMAIL</label>
                      <input type="email" class="form-control" name="<?=COL_EMAIL?>" value="<?= $edit ? $data[COL_EMAIL] : ""?>" placeholder="Email" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <label>NAMA</label>
                        <input type="text" class="form-control" name="<?=COL_NAME?>" value="<?= $edit ? $data[COL_NAME] : ""?>" placeholder="Nama Pengguna" required>
                      </div>
                      <div class="col-sm-6">
                        <label>KATEGORI</label>
                        <select name="<?=COL_ROLEID?>" class="form-control" required>
                          <?=GetCombobox("SELECT * FROM _roles", COL_ROLEID, COL_ROLENAME, (!empty($data[COL_ROLEID]) ? $data[COL_ROLEID] : null))?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div id="div-unit" class="form-group">
                    <label>UNIT</label>
                    <select name="<?=COL_COMPANYID?>" class="form-control" style="width: 100% !important">
                      <?=GetCombobox("SELECT * FROM mskpd", COL_SKPDID, COL_SKPDNAMA, (!empty($data[COL_COMPANYID]) ? $data[COL_COMPANYID] : null))?>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6 pl-3">
                  <div class="form-group">
                    <label>PASSWORD</label>
                    <input type="password" class="form-control" name="<?=COL_PASSWORD?>" placeholder="*****" />
                  </div>
                  <div class="form-group">
                    <label>KONFIRMASI PASSWORD</label>
                    <input type="password" class="form-control" name="RepeatPassword" placeholder="*****" />
                  </div>
                  <?php
                  if(!$edit) {
                    ?>
                    <div class="form-group">
                      <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" name="UserStatus" id="checkStatus">
                        <label class="custom-control-label" for="checkStatus">TIDAK AKTIF</label>
                      </div>
                    </div>
                    <?php
                  }
                  ?>
                </div>
              </div>
            </div>
            <div class="card-footer text-center">
              <a href="<?=site_url('user/index')?>" class="btn btn-default"><i class="far fa-arrow-circle-left"></i>&nbsp;KEMBALI</a>&nbsp;
              <button type="submit" class="btn btn-primary"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
            </div>
            <?=form_close()?>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
  $('[name=UserStatus]').change(function(){
    if($(this).is(':checked')) {
      $('label[for=checkStatus]').html('AKTIF');
    } else {
      $('label[for=checkStatus]').html('TIDAK AKTIF');
    }
  }).trigger('change');

  $('[name=RoleID]').change(function(){
    if($(this).val()=='<?=ROLEADMIN?>') {
      $('#div-unit').hide();
    } else {
      $('#div-unit').show();
    }
  }).trigger('change');

  $('#form-main').validate({
    ignore: "[type=file]",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="fad fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success(res.success);
            if(res.redirect) {
              location.href = res.redirect;
            } else {
              setTimeout(function(){
                //location.reload();
              }, 1000);
            }
          }
        },
        error: function(data) {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.html(txtSubmit);
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
