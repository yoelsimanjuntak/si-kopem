<?php
$data = array();
$arr = array();
$i = 0;
foreach ($res as $d) {
  $arr[$i] = array(
    $d[COL_KEGKODE],
    $d[COL_KEGNAMA],
    '<button type="button" class="btn btn-xs btn-primary btn-select" data-uniq='.$d[COL_UNIQ].'><i class="far fa-check-circle"></i>&nbsp;PILIH</button>'
  );
  $i++;
}
$data = json_encode($arr);
?>
<table id="browse-kegiatan" class="table table-bordered table-hover">

</table>
<script type="text/javascript">
$(document).ready(function() {
    var dataTable = $('#browse-kegiatan').dataTable({
      "autoWidth" : false,
      "aaData": <?=$data?>,
      "iDisplayLength": 8,
      "dom":"R<'row'<'col-sm-12'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
      "order": [[ 1, "asc" ]],
      "columnDefs": [
        {"targets":[0], "className":'nowrap'},
        {"targets":[2], "className":'nowrap text-center'}
      ],
      "aoColumns": [
        {"sTitle": "Kode","bSortable":false,"width": "10px"},
        {"sTitle": "Nama","bSortable":false},
        {"sTitle": "Aksi","bSortable":false,"width": "50px"}
      ],
      "createdRow": function(row, data, dataIndex) {
        $('.btn-select', $(row)).click(function() {
          $('[name=KegUniq]').val($(this).data('uniq')).trigger('change');
          return false;
        });
      },
    });
});
</script>
