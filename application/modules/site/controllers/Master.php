<?php
class Master extends MY_Controller {
  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }
    if(GetLoggedUser()[COL_ROLEID]!=ROLEADMIN) {
      show_error('Maaf, anda tidak memiliki hak akses!');
      exit();
    }
  }

  public function rpjmd() {
    $data['title'] = "Sasaran RPJMD";
    $data['res'] = $this->db
    ->get(TBL_MRPJMD)
    ->result_array();

    $this->template->load('backend', 'site/master/rpjmd', $data);
  }

  public function rpjmd_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $dat = array(
        COL_SASARANNAMA=>$this->input->post(COL_SASARANNAMA),
        COL_CREATEDON=>date('Y-m-d H:i'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MRPJMD, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['title'] = 'Tambah Sasaran RPJMD';
      $this->load->view('site/master/rpjmd-form', $data);
    }
  }

  public function rpjmd_add_det($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $dat = array(
        COL_IDRPJMD=>$id,
        COL_INDIKATORNO=>$this->input->post(COL_INDIKATORNO),
        COL_INDIKATORURAIAN=>$this->input->post(COL_INDIKATORURAIAN),
        COL_INDIKATORTARGET=>$this->input->post(COL_INDIKATORTARGET),
        COL_INDIKATORSATUAN=>$this->input->post(COL_INDIKATORSATUAN),
        COL_INDIKATORSKOR=>6
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MRPJMD_DET, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db
        ->where(COL_UNIQ, $id)
        ->update(TBL_MRPJMD, array(
          COL_UPDATEDON=>date('Y-m-d H:i'),
          COL_UPDATEDBY=>$ruser[COL_USERNAME]
        ));

        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    }
  }

  public function rpjmd_edit($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_MRPJMD)->row_array();

    if(empty($rdata)) {
      ShowJsonError('PARAMETER TIDAK VALID.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_SASARANNAMA=>$this->input->post(COL_SASARANNAMA),
        COL_UPDATEDON=>date('Y-m-d H:i'),
        COL_UPDATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MRPJMD, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.', array('redirect'=>site_url('site/master/rpjmd')));
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['title'] = 'Ubah Sasaran RPJMD';
      $data['data'] = $rdata;
      $this->template->load('backend', 'site/master/rpjmd-detail', $data);
    }
  }

  public function rpjmd_delete($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MRPJMD)->row_array();
    if(empty($rdata)) {
      ShowJsonError('PARAMETER TIDAK VALID.');
      exit();
    }

    try {
      $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_MRPJMD);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      ShowJsonSuccess('HAPUS DATA BERHASIL.');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function rpjmd_delete_det($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MRPJMD_DET)->row_array();
    if(empty($rdata)) {
      ShowJsonError('PARAMETER TIDAK VALID.');
      exit();
    }

    try {
      $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_MRPJMD_DET);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $res = $this->db
      ->where(COL_UNIQ, $rdata[COL_IDRPJMD])
      ->update(TBL_MRPJMD, array(
        COL_UPDATEDON=>date('Y-m-d H:i'),
        COL_UPDATEDBY=>$ruser[COL_USERNAME]
      ));

      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      ShowJsonSuccess('HAPUS DATA BERHASIL.');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function rad() {
    $data['title'] = "Rencana Aksi Daerah (RAD)";
    $data['res'] = $this->db
    ->get(TBL_MRAD)
    ->result_array();

    $this->template->load('backend', 'site/master/rad', $data);
  }

  public function rad_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $dat = array(
        COL_RADJUDUL=>$this->input->post(COL_RADJUDUL),
        COL_CREATEDON=>date('Y-m-d H:i'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MRAD, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['title'] = 'Tambah RAD';
      $this->load->view('site/master/rad-form', $data);
    }
  }

  public function rad_add_det($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $dat = array(
        COL_IDRAD=>$id,
        COL_RADINDIKATORNO=>$this->input->post(COL_RADINDIKATORNO),
        COL_RADINDIKATORURAIAN=>$this->input->post(COL_RADINDIKATORURAIAN),
        COL_RADINDIKATORMASALAH=>$this->input->post(COL_RADINDIKATORMASALAH),
        COL_RADSKOR=>4
        //COL_RADTARGET=>$this->input->post(COL_RADTARGET),
        //COL_RADSATUAN=>$this->input->post(COL_RADSATUAN)
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MRAD_DET, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $res = $this->db
        ->where(COL_UNIQ, $id)
        ->update(TBL_MRAD, array(
          COL_UPDATEDON=>date('Y-m-d H:i'),
          COL_UPDATEDBY=>$ruser[COL_USERNAME]
        ));

        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    }
  }

  public function rad_edit($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_MRAD)->row_array();

    if(empty($rdata)) {
      ShowJsonError('PARAMETER TIDAK VALID.');
      exit();
    }

    if(!empty($_POST)) {
      $dat = array(
        COL_RADJUDUL=>$this->input->post(COL_RADJUDUL),
        COL_RADRUMUSAN=>$this->input->post(COL_RADRUMUSAN),
        COL_RADSTRATEGI=>$this->input->post(COL_RADSTRATEGI),
        COL_UPDATEDON=>date('Y-m-d H:i'),
        COL_UPDATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MRAD, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.', array('redirect'=>site_url('site/master/rad')));
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['title'] = 'Ubah RAD';
      $data['data'] = $rdata;
      $this->template->load('backend', 'site/master/rad-detail', $data);
    }
  }

  public function rad_delete($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MRAD)->row_array();
    if(empty($rdata)) {
      ShowJsonError('PARAMETER TIDAK VALID.');
      exit();
    }

    try {
      $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_MRAD);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      ShowJsonSuccess('HAPUS DATA BERHASIL.');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function rad_delete_det($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_MRAD_DET)->row_array();
    if(empty($rdata)) {
      ShowJsonError('PARAMETER TIDAK VALID.');
      exit();
    }

    try {
      $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_MRAD_DET);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $res = $this->db
      ->where(COL_UNIQ, $rdata[COL_IDRAD])
      ->update(TBL_MRAD, array(
        COL_UPDATEDON=>date('Y-m-d H:i'),
        COL_UPDATEDBY=>$ruser[COL_USERNAME]
      ));

      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      ShowJsonSuccess('HAPUS DATA BERHASIL.');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }
}
