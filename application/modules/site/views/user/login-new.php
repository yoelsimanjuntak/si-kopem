
<!DOCTYPE html>
<html class="h-100" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title><?=!empty($title) ? $this->setting_web_name.' - '.$title : $this->setting_web_name?></title>
    <link rel="stylesheet" href="<?=base_url()?>assets/fonts/css/font-awesome.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/fonts/fontawesome-pro/web/css/all.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/adminlte-new/dist/css/adminlte.min.css">
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?=base_url()?>assets/js/jquery.particleground.js"></script>
    <link rel="icon" type="image/png" href=<?=MY_IMAGEURL.$this->setting_web_logo?>>

    <style>
    .login,
    .image {
        min-height: 100vh;
    }
    .bg-image {
        background-image: url('<?=MY_IMAGEURL.'bg-login.png'?>');
        background-size: cover;
        background-position: center center;
    }
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?=base_url()?>assets/media/preloader/<?=$this->setting_web_preloader?>') center no-repeat #fff;
    }
    .login-page::after {
      /*background: url(http://localhost/hh-bmkg//assets/media/image/footer-map-bg.png) no-repeat scroll center center / 75% auto;*/
      /*background: url('<?=base_url()?>assets/media/image/footer-map-bg.png');*/
      content: "";
      height: 100%;
      left: 0;
      opacity: 0.1;
      position: absolute;
      top: 0;
      width: 100%;
      z-index: -1;
      opacity: 0.1;
      background-color: #ffeb3b;
    }
    </style>
</head>
<script>
$(window).load(function() {
    $(".se-pre-con").fadeOut("slow");
});
</script>
<body class="h-100 login-page">
  <div class="se-pre-con"></div>
    <div class="container-fluid">
        <div class="row no-gutter">
            <div class="col-md-6 d-none d-md-flex bg-image"></div>
            <div class="col-md-6 bg-light">
                <div class="login d-flex align-items-center py-5">
                    <div class="container">
                      <div class="row">
                        <div class="col-lg-10 col-xl-7 mx-auto">
                          <div class="login-box">
                            <div class="card" style="background: #fff">
                                <div class="card-body login-card-body" style="background: none">
                                  <div class="login-logo">
                                      <img class="user-image mb-3" src="<?=MY_IMAGEURL.$this->setting_web_logo?>" style="width: 60px; height: 60px" alt="Logo"><br  />
                                      <a href="<?=site_url()?>">
                                          <h5 class="mb-0 font-weight-light"><?=strtoupper($this->setting_web_name)?></h5>
                                          <p class="font-weight-bold" style="font-size: 10pt;"><?=nl2br($this->setting_web_desc)?></p>
                                      </a>
                                  </div>
                                  <?php  if($this->input->get('msg') == 'notmatch'){ ?>
                                    <p class="text-danger text-center">
                                      Maaf, username / password anda tidak tepat.
                                    </p>
                                  <?php } ?>

                                  <?php  if($this->input->get('msg') == 'suspend'){ ?>
                                    <p class="text-danger text-center">
                                      Maaf, akun anda sedang di suspend. Silakan hubungi administrator untuk informasi lebih lanjut
                                    </p>
                                    <div class="callout callout-danger">
                                      <span class="text-danger"><i class="fad fa-ban"></i>&nbsp;&nbsp;Akun anda disuspend.</span>
                                    </div>
                                  <?php } ?>

                                  <?php  if($this->input->get('msg') == 'captcha'){ ?>
                                    <p class="text-danger text-center">
                                      Maaf, CAPTCHA yang anda masukkan tidak sesuai. Silakan coba kembali.
                                    </p>
                                  <?php } ?>
                                  <?= form_open(current_url(),array('id'=>'validate')) ?>
                                  <div class="input-group mb-3">
                                    <input type="text" class="form-control" name="<?=COL_USERNAME?>" placeholder="Username" required>
                                    <div class="input-group-append">
                                      <div class="input-group-text">
                                        <span class="fad fa-user"></span>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="input-group mb-3">
                                      <input type="password" class="form-control" name="<?=COL_PASSWORD?>" placeholder="Password" required>
                                      <div class="input-group-append">
                                        <div class="input-group-text">
                                          <span class="fad fa-key"></span>
                                        </div>
                                      </div>
                                  </div>
                                  <div class="footer" style="text-align: right;">
                                    <button type="submit" class="btn btn-primary btn-block btn-round">MASUK&nbsp;<i class="far fa-sign-in"></i></button>
                                  </div>
                                  <?= form_close(); ?>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
