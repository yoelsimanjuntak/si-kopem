<?php
class Kegiatan extends MY_Controller {
  public function __construct()
  {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }
  }

  public function index() {
    $data['title'] = "Kegiatan / Sub Kegiatan";
    $this->template->load('backend', 'site/kegiatan/index', $data);
  }

  public function index_load() {
    $ruser = GetLoggedUser();
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $filterSkpd = !empty($_POST['filterSkpd'])?$_POST['filterSkpd']:null;

    $orderdef = array(COL_SKPDNAMA=>'asc');
    $orderables = array(null,COL_KEGTAHUN,COL_KEGKODE,COL_KEGNAMA,COL_KEGANGGARAN,COL_CREATEDON);
    $cols = array(COL_KEGKODE,COL_KEGNAMA);
    $condSkpd = "1=1";

    if(!empty($filterSkpd)) {
      $condSkpd = "IdSkpd=".$filterSkpd;
    } else {
      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        $condSkpd = "IdSkpd=".$ruser[COL_COMPANYID];
      }
    }

    $queryAll = $this->db->where($condSkpd)->get(TBL_TKEGIATAN);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->select('tkegiatan.*, _userinformation.Name, _userinformation.Name, mskpd.SkpdNama')
    ->where($condSkpd)
    ->join(TBL__USERINFORMATION,TBL__USERINFORMATION.'.'.COL_USERNAME." = ".TBL_TKEGIATAN.".".COL_CREATEDON,"left")
    ->join(TBL_MSKPD,TBL_MSKPD.'.'.COL_SKPDID." = ".TBL_TKEGIATAN.".".COL_IDSKPD,"left")
    ->order_by(COL_CREATEDON, 'desc')
    ->get_compiled_select(TBL_TKEGIATAN, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start")->result_array();
    $data = [];

    foreach($rec as $r) {
      $data[] = array(
        '<a class="btn btn-xs btn-danger btn-action" href="'.site_url('site/kegiatan/delete/'.$r[COL_KEGID]).'"><i class="far fa-times-circle"></i></a>&nbsp;'.
        '<a class="btn btn-xs btn-primary" href="'.site_url('site/kegiatan/edit/'.$r[COL_KEGID]).'"><i class="far fa-search"></i></a>',
        $r[COL_KEGTAHUN],
        $r[COL_KEGKODE],
        $r[COL_KEGNAMA],
        number_format($r[COL_KEGANGGARAN]),
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function browse() {
    $_urusan = $this->input->post('Urusan');
    $res = $this->db
    ->where(COL_KEGURUSAN, $_urusan)
    ->get(TBL_MKEGIATAN)
    ->result_array();
    $this->load->view('site/kegiatan/browse', array('res'=>$res));
  }

  public function browse_iku() {
    $_id = $this->input->post(COL_IDRPJMD);
    $res = $this->db
    ->where(COL_IDRPJMD, $_id)
    ->order_by(COL_INDIKATORNO, 'asc')
    ->get(TBL_MRPJMD_DET)
    ->result_array();

    $html = '';
    if($res) {
      foreach($res as $r) {
        $html .= @"
        <tr>
          <td>".$r[COL_INDIKATORNO]."</td>
          <td>".$r[COL_INDIKATORURAIAN]."</td>
          <td>".$r[COL_INDIKATORSATUAN]."</td>
          <td>".$r[COL_INDIKATORTARGET]."</td>
          <td style='white-space: nowrap'><button class='btn btn-primary btn-xs btn-select' data-uniq='".$r[COL_UNIQ]."' data-uraian='".$r[COL_INDIKATORURAIAN]."'><i class='far fa-plus-circle'></i>&nbsp;TAMBAH</button></td>
        </tr>
        ";
      }
    } else {
      $html .= '<tr><td colspan="5" class="text-center">DATA TIDAK TERSEDIA</td></tr>';
    }
    echo $html;
  }

  public function browse_rad() {
    $_id = $this->input->post(COL_IDRAD);
    $res = $this->db
    ->where(COL_IDRAD, $_id)
    ->order_by(COL_RADINDIKATORNO, 'asc')
    ->get(TBL_MRAD_DET)
    ->result_array();

    $html = '';
    if($res) {
      foreach($res as $r) {
        $html .= @"
        <tr>
          <td>".$r[COL_RADINDIKATORNO]."</td>
          <td>".$r[COL_RADINDIKATORURAIAN]."</td>
          <td style='white-space: nowrap'><button class='btn btn-primary btn-xs btn-select' data-uniq='".$r[COL_UNIQ]."' data-uraian='".$r[COL_RADINDIKATORURAIAN]."'><i class='far fa-plus-circle'></i>&nbsp;TAMBAH</button></td>
        </tr>
        ";
      }
    } else {
      $html .= '<tr><td colspan="5" class="text-center">DATA TIDAK TERSEDIA</td></tr>';
    }
    echo $html;
  }

  public function get() {
    $_uniq = $this->input->get(COL_UNIQ);
    $res = $this->db
    ->where(COL_UNIQ, $_uniq)
    ->get(TBL_MKEGIATAN)
    ->row_array();

    echo json_encode($res);
  }

  public function add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $detIKU = json_decode($this->input->post('KegIKU'));
      $detRAD = json_decode($this->input->post('KegRAD'));
      $arrIKU = array();
      $arrRAD = array();

      $dat = array(
        COL_IDSKPD=>$this->input->post(COL_IDSKPD),
        COL_KEGTAHUN=>$this->input->post(COL_KEGTAHUN),
        COL_KEGKODE=>$this->input->post(COL_KEGKODE),
        COL_KEGNAMA=>$this->input->post(COL_KEGNAMA),
        COL_KEGURUSAN=>$this->input->post(COL_KEGURUSAN),
        COL_KEGSASARAN=>$this->input->post(COL_KEGSASARAN),
        COL_KEGINDIKATOR=>$this->input->post(COL_KEGINDIKATOR),
        COL_KEGSATUAN=>$this->input->post(COL_KEGSATUAN),
        COL_KEGTARGET=>$this->input->post(COL_KEGTARGET),
        COL_KEGANGGARAN=>toNum($this->input->post(COL_KEGANGGARAN)),

        COL_CREATEDON=>date('Y-m-d H:i'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TKEGIATAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $idKeg = $this->db->insert_id();
        if(!empty($detIKU)) {
          foreach($detIKU as $d) {
            $_bobot = 1;
            if($d->IndikatorLevel=='TINGGI') $_bobot = 1.5;
            else if($d->IndikatorLevel=='RENDAH') $_bobot = 0.5;

            $arrIKU[] = array(
              COL_IDKEG=>$idKeg,
              COL_IDRPJMD=>$d->Uniq,
              //COL_SKOR=>6,
              COL_LEVEL=>$d->IndikatorLevel,
              COL_BOBOT=>$_bobot,
            );
          }
        }
        if(!empty($detRAD)) {
          foreach($detRAD as $d) {
            $_bobot = 1;
            if($d->RADIndikatorLevel=='TINGGI') $_bobot = 1.5;

            $arrRAD[] = array(
              COL_IDKEG=>$idKeg,
              COL_IDRAD=>$d->Uniq,
              //COL_SKOR=>4
              COL_LEVEL=>$d->RADIndikatorLevel,
              COL_BOBOT=>$_bobot,
            );
          }
        }

        if(!empty($arrIKU)) {
          $res = $this->db->insert_batch(TBL_TKEGIATAN_RPJMD, $arrIKU);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }

        if(!empty($arrRAD)) {
          $res = $this->db->insert_batch(TBL_TKEGIATAN_RAD, $arrRAD);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.', array('redirect'=>site_url('site/kegiatan/index')));
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['title'] = 'Tambah Kegiatan / Sub Kegiatan';
      $this->template->load('backend', 'site/kegiatan/form', $data);
    }
  }

  public function edit($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->where(COL_KEGID, $id)
    ->get(TBL_TKEGIATAN)
    ->row_array();

    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    if(!empty($_POST)) {
      $detIKU = json_decode($this->input->post('KegIKU'));
      $detRAD = json_decode($this->input->post('KegRAD'));
      $arrIKU = array();
      $arrRAD = array();

      $dat = array(
        COL_IDSKPD=>$this->input->post(COL_IDSKPD),
        COL_KEGTAHUN=>$this->input->post(COL_KEGTAHUN),
        COL_KEGKODE=>$this->input->post(COL_KEGKODE),
        COL_KEGNAMA=>$this->input->post(COL_KEGNAMA),
        COL_KEGURUSAN=>$this->input->post(COL_KEGURUSAN),
        COL_KEGSASARAN=>$this->input->post(COL_KEGSASARAN),
        COL_KEGINDIKATOR=>$this->input->post(COL_KEGINDIKATOR),
        COL_KEGSATUAN=>$this->input->post(COL_KEGSATUAN),
        COL_KEGTARGET=>$this->input->post(COL_KEGTARGET),
        COL_KEGANGGARAN=>toNum($this->input->post(COL_KEGANGGARAN)),

        COL_UPDATEDON=>date('Y-m-d H:i'),
        COL_UPDATEDBY=>$ruser[COL_USERNAME]
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_KEGID, $id)->update(TBL_TKEGIATAN, $dat);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        if(!empty($detIKU)) {
          foreach($detIKU as $d) {
            $_bobot = 1;
            if($d->IndikatorLevel=='TINGGI') $_bobot = 1.5;
            else if($d->IndikatorLevel=='RENDAH') $_bobot = 0.5;

            $arrIKU[] = array(
              COL_IDKEG=>$id,
              COL_IDRPJMD=>$d->Uniq,
              //COL_SKOR=>6
              COL_LEVEL=>$d->IndikatorLevel,
              COL_BOBOT=>$_bobot,
            );
          }
        }
        if(!empty($detRAD)) {
          foreach($detRAD as $d) {
            $_bobot = 1;
            if($d->RADIndikatorLevel=='TINGGI') $_bobot = 1.5;

            $arrRAD[] = array(
              COL_IDKEG=>$id,
              COL_IDRAD=>$d->Uniq,
              //COL_SKOR=>4
              COL_LEVEL=>$d->RADIndikatorLevel,
              COL_BOBOT=>$_bobot,
            );
          }
        }

        if(!empty($arrIKU)) {
          $res = $this->db->where(COL_IDKEG, $id)->delete(TBL_TKEGIATAN_RPJMD);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }

          $res = $this->db->insert_batch(TBL_TKEGIATAN_RPJMD, $arrIKU);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }

        if(!empty($arrRAD)) {
          $res = $this->db->where(COL_IDKEG, $id)->delete(TBL_TKEGIATAN_RAD);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }

          $res = $this->db->insert_batch(TBL_TKEGIATAN_RAD, $arrRAD);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception('Error: '.$err['message']);
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('ENTRI DATA BERHASIL.', array('redirect'=>site_url('site/kegiatan/index')));
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      $data['title'] = 'Edit Kegiatan / Sub Kegiatan';
      $data['data'] = $rdata;
      $this->template->load('backend', 'site/kegiatan/form', $data);
    }
  }

  public function delete($id) {
    $ruser = GetLoggedUser();
    $rdata = $this->db->where(COL_KEGID, $id)->get(TBL_TKEGIATAN)->row_array();
    if(empty($rdata)) {
      ShowJsonError('PARAMETER TIDAK VALID.');
      exit();
    }

    try {
      $res = $this->db->where(COL_KEGID, $id)->delete(TBL_TKEGIATAN);
      if(!$res) {
        $err = $this->db->error();
        throw new Exception('Error: '.$err['message']);
      }

      $this->db->trans_commit();
      ShowJsonSuccess('HAPUS DATA BERHASIL.');
      return;
    } catch(Exception $ex) {
      $this->db->trans_rollback();
      ShowJsonError($ex->getMessage());
      return;
    }
  }

  public function skoring() {
    $ruser = GetLoggedUser();
    $data['getSKPD'] = $getSKPD = $this->input->get(COL_IDSKPD)?$this->input->get(COL_IDSKPD):null;
    $data['getTahun'] = $getTahun = $this->input->get('Tahun')?$this->input->get('Tahun'):date('Y');

    $idOPD = $getSKPD;
    $tahun = $getTahun;
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $idOPD = $ruser[COL_COMPANYID];
    }

    if(!empty($tahun)) {
      $condSkpd = " 1=1 ";
      if(!empty($idOPD)) {
        $condSkpd .= "and tkegiatan.IdSkpd = ".$idOPD;
      }

      $q = @"
      select * from  (
        select
          tkegiatan.*,
          mskpd.SkpdNama,
          IFNULL(
            (select sum(rpjmd.IndikatorSkor*detrpjmd.Bobot) from tkegiatan_rpjmd detrpjmd left join mrpjmd_det rpjmd on rpjmd.Uniq=detrpjmd.idRPJMD where detrpjmd.IdKeg = tkegiatan.KegID),0) + IFNULL((select sum(rad.RADSkor*detrad.Bobot) from tkegiatan_rad detrad left join mrad_det rad on rad.Uniq = detrad.IdRAD where detrad.IdKeg = tkegiatan.KegID)
            ,0) as Skor
        from tkegiatan
        left join mskpd on mskpd.SkpdId = tkegiatan.IdSkpd
        where $condSkpd and KegTahun = $tahun
      ) tbl
      order by tbl.Skor desc, tbl.SkpdNama asc
      ";
      $res = $this->db->query($q)->result_array();
      /*->select('tkegiatan.*, mskpd.SkpdNama, IFNULL((select sum(Skor) from tkegiatan_rpjmd detrpjmd where detrpjmd.IdKeg = tkegiatan.KegID),0) + IFNULL((select sum(Skor) from tkegiatan_rad detrad where detrad.IdKeg = tkegiatan.KegID),0) as Skor')
      ->join(TBL_MSKPD,TBL_MSKPD.'.'.COL_SKPDID." = ".TBL_TKEGIATAN.".".COL_IDSKPD,"left")
      ->where($condSkpd)
      ->order_by('IFNULL(select sum(Skor) from tkegiatan_rad detrad where detrad.IdKeg = tkegiatan.KegID, 0)', 'desc')
      ->order_by(COL_KEGNAMA, 'asc')
      ->get(TBL_TKEGIATAN)
      ->result_array();*/

      $data['res'] = $res;
    }

    $data['title'] = "Analisis / Skoring Kegiatan";
    $this->template->load('backend', 'site/kegiatan/skoring', $data);
  }

  public function skoring_skpd() {
    $ruser = GetLoggedUser();
    $data['getSKPD'] = $getSKPD = $this->input->get(COL_IDSKPD)?$this->input->get(COL_IDSKPD):null;
    $data['getTahun'] = $getTahun = $this->input->get('Tahun')?$this->input->get('Tahun'):date('Y');

    $idOPD = $getSKPD;
    $tahun = $getTahun;
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $idOPD = $ruser[COL_COMPANYID];
    }

    if(!empty($tahun)) {
      $condSkpd = " 1=1 ";
      if(!empty($idOPD)) {
        $condSkpd .= "and tkegiatan.IdSkpd = ".$idOPD;
      }

      $q = @"
      select * from  (
        select
          #tkegiatan.*,
          mskpd.SkpdNama,
          sum(tkegiatan.KegAnggaran) as KegAnggaran,
          sum(IFNULL(
            (select sum(rpjmd.IndikatorSkor*detrpjmd.Bobot) from tkegiatan_rpjmd detrpjmd left join mrpjmd_det rpjmd on rpjmd.Uniq=detrpjmd.idRPJMD where detrpjmd.IdKeg = tkegiatan.KegID),0) + IFNULL((select sum(rad.RADSkor*detrad.Bobot) from tkegiatan_rad detrad left join mrad_det rad on rad.Uniq = detrad.IdRAD where detrad.IdKeg = tkegiatan.KegID)
            ,0)) as Skor
        from tkegiatan
        left join mskpd on mskpd.SkpdId = tkegiatan.IdSkpd
        where $condSkpd and KegTahun = $tahun
        group by mskpd.SkpdNama
      ) tbl
      order by tbl.Skor desc, tbl.SkpdNama asc
      ";
      $res = $this->db->query($q)->result_array();
      /*->select('tkegiatan.*, mskpd.SkpdNama, IFNULL((select sum(Skor) from tkegiatan_rpjmd detrpjmd where detrpjmd.IdKeg = tkegiatan.KegID),0) + IFNULL((select sum(Skor) from tkegiatan_rad detrad where detrad.IdKeg = tkegiatan.KegID),0) as Skor')
      ->join(TBL_MSKPD,TBL_MSKPD.'.'.COL_SKPDID." = ".TBL_TKEGIATAN.".".COL_IDSKPD,"left")
      ->where($condSkpd)
      ->order_by('IFNULL(select sum(Skor) from tkegiatan_rad detrad where detrad.IdKeg = tkegiatan.KegID, 0)', 'desc')
      ->order_by(COL_KEGNAMA, 'asc')
      ->get(TBL_TKEGIATAN)
      ->result_array();*/

      $data['res'] = $res;
    }

    $data['title'] = "Analisis / Skoring Kegiatan";
    $this->template->load('backend', 'site/kegiatan/skoring-skpd', $data);
  }

  public function bobot_iku() {
    $ruser = GetLoggedUser();
    $data['getSKPD'] = $getSKPD = $this->input->get(COL_IDSKPD)?$this->input->get(COL_IDSKPD):null;
    $data['getTahun'] = $getTahun = $this->input->get('Tahun')?$this->input->get('Tahun'):date('Y');

    $idOPD = $getSKPD;
    $tahun = $getTahun;
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $idOPD = $ruser[COL_COMPANYID];
    }

    if(!empty($tahun)) {
      $condSkpd = " 1=1 ";
      if(!empty($idOPD)) {
        $condSkpd .= "and keg.IdSkpd = ".$idOPD;
      }

      $q = @"
      select * from (
      select
        rpjmd.SasaranNama,
        ikudet.IndikatorUraian,
        ikudet.Uniq as IndikatorId,
        sum(keg.KegAnggaran) as KegAnggaran
      from tkegiatan_rpjmd iku
      left join tkegiatan keg on keg.KegID = iku.IdKeg
      left join mrpjmd_det ikudet on ikudet.Uniq = iku.IdRPJMD
      left join mrpjmd rpjmd on rpjmd.Uniq = ikudet.IdRPJMD
      where $condSkpd and KegTahun = $tahun
      group by rpjmd.SasaranNama,ikudet.IndikatorUraian,ikudet.Uniq
      ) tbl
      order by tbl.KegAnggaran desc
      ";
      $res = $this->db->query($q)->result_array();

      $data['res'] = $res;
    }

    $data['title'] = "Bobot Anggaran per IKU";
    $this->template->load('backend', 'site/kegiatan/bobot-iku', $data);
  }

  public function bobot_rad() {
    $ruser = GetLoggedUser();
    $data['getSKPD'] = $getSKPD = $this->input->get(COL_IDSKPD)?$this->input->get(COL_IDSKPD):null;
    $data['getTahun'] = $getTahun = $this->input->get('Tahun')?$this->input->get('Tahun'):date('Y');

    $idOPD = $getSKPD;
    $tahun = $getTahun;
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $idOPD = $ruser[COL_COMPANYID];
    }

    if(!empty($tahun)) {
      $condSkpd = " 1=1 ";
      if(!empty($idOPD)) {
        $condSkpd .= "and keg.IdSkpd = ".$idOPD;
      }

      $q = @"
      select * from (
        select
          mrad.RADJudul,
          mraddet.RADIndikatorUraian,
          sum(keg.KegAnggaran) as KegAnggaran
        from tkegiatan_rad rad
        left join tkegiatan keg on keg.KegID = rad.IdKeg
        left join mrad_det mraddet on mraddet.Uniq = rad.IdRAD
        left join mrad mrad on mrad.Uniq = mraddet.IdRAD
        where $condSkpd and KegTahun = $tahun
        group by mrad.RADJudul,mraddet.RADIndikatorUraian
      ) tbl
      order by tbl.KegAnggaran desc
      ";
      $res = $this->db->query($q)->result_array();

      $data['res'] = $res;
    }

    $data['title'] = "Bobot Anggaran per RAD";
    $this->template->load('backend', 'site/kegiatan/bobot-rad', $data);
  }
}
